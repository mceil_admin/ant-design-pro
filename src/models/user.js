import { query as queryUsers, queryCurrent,queryCurrentRoutes } from '@/services/user';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
    currentRoutes:[],
  },
  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const response = yield call(queryCurrent);
      yield put({
        type: 'saveCurrentUser',
        payload: response,
      });
    },
    *fetchCurrentRoutes(_, { call, put }) {
      const response = yield call(queryCurrentRoutes);
      console.log("fetchCurrentRoutes",response);
      yield put({
        type: 'saveCurrentRoutes',
        payload: response,
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    saveCurrentRoutes(state, action) {
      console.log("saveCurrentRoutes",state, action)
      console.log("action.payload.routes",action.payload.routes[0])
      return {
        ...state,
        currentRoutes: action.payload.routes || [],
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};
