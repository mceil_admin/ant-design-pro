/* eslint-disable import/no-mutable-exports */
let CURRENT = 'NULL';
/**
 * use  authority or getAuthority
 * @param {string|()=>String} currentAuthority
 */
const renderAuthorize = Authorized => currentAuthority => {
  console.log("1234",currentAuthority);
  // 如果是清了缓存在刷新登录页面就会显示为admin,如果退出后直接刷新则会显示guest 这个问题的关键在 utils/authority.js
  if (currentAuthority) {
    if (typeof currentAuthority === 'function') {
      CURRENT = currentAuthority();
    }
    console.log("Object.prototype.toString.call(currentAuthority)",Object.prototype.toString.call(currentAuthority));
    // Object.prototype.toString.call(["admin"]) // "[object Array]"
    if (
      Object.prototype.toString.call(currentAuthority) === '[object String]' ||
      Array.isArray(currentAuthority)
    ) {
      CURRENT = currentAuthority;
    }
  } else {
    CURRENT = 'NULL';
  }
  return Authorized;
};

export { CURRENT };
export default Authorized => renderAuthorize(Authorized);
