import CheckPermissions from './CheckPermissions';

const Authorized = ({ children, authority, noMatch = null }) => {
  const childrenRender = typeof children === 'undefined' ? null : children;
  console.log(11111,authority, childrenRender, noMatch);
  return CheckPermissions(authority, childrenRender, noMatch);
};

export default Authorized;
